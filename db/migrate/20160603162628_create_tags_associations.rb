class CreateTagsAssociations < ActiveRecord::Migration
  def change
    create_table :tags_associations do |t|
      t.belongs_to :tag, index: true
      t.references :taggable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
