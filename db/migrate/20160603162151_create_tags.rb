class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :title
      t.boolean :status
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
