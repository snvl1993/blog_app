class CreateMarks < ActiveRecord::Migration
  def change
    create_table :marks do |t|
      t.integer :markable_id
      t.string :markable_type
      t.string :mark_type
      t.string :clicked_button
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
