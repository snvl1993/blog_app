class ConnectBlogsWithUsers < ActiveRecord::Migration
  def change
    change_table :blogs do |t|
      t.belongs_to :user, index: true
    end
  end
end