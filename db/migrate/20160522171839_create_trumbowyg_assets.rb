class CreateTrumbowygAssets < ActiveRecord::Migration
  def change
    create_table :trumbowyg_assets do |t|
      t.string :file, null: false
      t.string :digest, null: false

      t.timestamps
    end
  end
end