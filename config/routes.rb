Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  get 'pagination/change'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  mount Peek::Railtie => '/peek'

  get 'welcome/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  concern :commentable do
    resources :comments, shallow: true do
      member do
        get 'reply', to: 'comments#new_reply'
        post 'reply', to: 'comments#create_reply'
      end
    end
  end

  resources :tags do
    member do
      put 'approve', to: 'tags#approve'
      put 'disapprove', to: 'tags#disapprove'
    end
  end

  resources :marks, only: [:create, :destroy]
  post 'marks/:mark_type/handle', to: 'marks#handle'

  resources :articles, except: :index, concerns: [:commentable]

  resources :blogs do
    resources :articles, only: :index
  end

  # Action where user will be redirected after authorization
  get 'users/profile', as: 'user_root'
  get 'users/:user_id/profile', to: 'users#profile'

  # resource :users
  get 'user/edit', to: 'users#edit'

  # Pagination type change
  post 'pagination/change', to: 'pagination#change'

  # Route for trumbowyg file uploading.
  post 'trumbowig/file-upload', to: 'trumbowyg/trumbowyg#file_upload'

  devise_for :users, controllers: {registrations: 'registrations'}, path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register'}
  devise_scope :user do
    post 'profile/image', to: 'registrations#profile_image_upload'
  end
end
