# @todo: Improve marks configuration and make it more convenient.
# Maybe create own configuration object or store settings in YAML etc.
# Use default values for settings.

Rails.configuration.mark_model_associations = {
    article: [:like, :average_rating],
    comment: [:like, :rating]
}

Rails.configuration.mark_properties = {
    rating: {
        counting_method: :sum,
        buttons: {
            like: {
                value: 1
            },
            dislike: {
                value: -1
            },
        }
    },
    like: {
        counting_method: :sum,
        buttons: {
            add: {
                value: 1,
            },
        },
    },
    average_rating: {
        title: 'Average',
        counting_method: :average,
        buttons: {
            r1: {
                tooltip: 'Miserably',
                value: 1,
            },
            r2: {
                tooltip: 'Bad',
                value: 2,
            },
            r3: {
                tooltip: 'Not bad',
                value: 3,
            },
            r4: {
                tooltip: 'Good!',
                value: 4,
            },
            r5: {
                tooltip: 'Excellent!',
                value: 5,
            },
        }
    }
}