RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'Article' do
    edit do
      configure :text, :ck_editor
    end
  end

  config.model 'Blog' do
    edit do
      configure :description, :ck_editor
    end
  end

  config.model 'User' do
    edit do
      configure :role_assignments do
        hide
      end
      configure :roles do
        # Hide roles field id user can not assign roles
        visible do
          Ability.new(bindings[:view]._current_user).can? :assign_roles, User
        end
      end
    end
  end
end
