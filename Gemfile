source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'
# Use sqlite3 as the database for Active Record
# gem 'sqlite3'

# Use Postgres as the database for Active Record.
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks', '~> 5.0.0.beta'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Administration gem.
gem 'rails_admin'

# Authorization
gem 'devise'

# Use cancan for managing user roles
gem 'cancancan'

# Facebook, Twitter, etc. authorization
gem 'omniauth'

# Gem which performs environment variables
gem 'figaro'

# Development peek bar gems
gem 'peek'
gem 'peek-performance_bar'
gem 'peek-host'
gem 'peek-git'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  # gem 'byebug'

  # IDE debug gems
  gem 'debase'
  gem 'ruby-debug-ide'

  # Pretty objects console output gem
  gem 'awesome_print', require: 'ap'

  # Gem for Google Chrome extension, that gives a lot of development goodies.
  gem 'meta_request'

  # Make Rails server console output better.
  gem 'quiet_assets'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Gem which can count used memory.
  gem 'get_process_mem'
end

gem 'newrelic_rpm'

# HTML editors gems. CKEditor is for admin use (rails admin works only with CKEditor and Froala)
# Trumbowyg is for common usage, because it lighter then CKE.
gem 'ckeditor'
gem 'trumbowyg2-rails', '>= 2.1.0'

gem 'normalize-rails'

gem 'bootstrap-sass'

# HAML gems. It is a convenient way to make templates.
gem 'haml'
gem 'haml-rails', '~> 0.9'

# Gem for creating navigation menus.
gem 'navigasmic'

# Pagination gem.
gem 'kaminari'
gem 'bootstrap-kaminari-views'

gem 'jquery-infinite-pages'

# This gem is required for Heroku.
gem 'rails_12factor', group: :production

# Ruby wrapper for ImageMagic.
gem 'mini_magick'

# Gem for file upload and store.
gem 'carrierwave'

# Bootstrap file field.
gem 'bootstrap-filestyle-rails'

# AJAX file upload.
gem 'remotipart'

# Gem for building gravatar images. Look at https://www.gravatar.com/
gem 'gravatarify'

gem 'nokogiri'
gem 'html_truncator'

gem 'js-routes'

gem 'rails-enquirejs'

# Specify the ruby version.
ruby '2.2.0'