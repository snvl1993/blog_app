class PaginationController < ApplicationController
  def change
    [:pages, :infinite].each do |el|
      if params.include? el
        session[:pagination_type] = el
      end
    end

    redirect_to :back
  end
end
