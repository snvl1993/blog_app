class CommentsController < ApplicationController
  # Set resources which may be parent fo current controller.
  parent_resources :article

  layout :layout

  def index
    @commentable = parent_object
    @comments = @commentable.comments.page params[:page]
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def new
    @commentable = parent_object
    @comment = @commentable.comments.new
  end

  def create
    @commentable = parent_object
    @comment = @commentable.comments.new comments_params
    @comment.user = current_user

    if @comment.save
      if request.xhr?
        render 'create'
      else
        redirect_to url_for(@commentable)
      end
    else
      render 'new'
    end
  end

  def edit
    @comment = Comment.find(params[:id])
    unless can? :edit, @comment
      raise CanCan::AccessDenied.new('You are not permitted to edit this comment!', :edit, @comment)
    end
  end

  def update
    @comment = Comment.find(params[:id])

    if @comment.update(comments_params)
      redirect_to @comment
    else
      render 'edit'
    end
  end

  def new_reply
    @commentable = Comment.find params[:id]
    @comment = @commentable.comments.new
  end

  def create_reply
    commentable = Comment.find params[:id]
    @comment = commentable.comments.new comments_params
    @comment.user = current_user

    if @comment.save
      if request.xhr?
        render commentable
      else
        redirect_to comment_path(commentable)
      end
    else
      render 'new_reply'
    end
  end

  def destroy
    @comment = Comment.find params[:id]

    if can? :destroy, @comment
      @comment.destroy
    else
      raise CanCan::AccessDenied.new('You are not permitted to destroy this comment!', :destroy, @comment)
    end

    redirect_to @comment.commentable
  end

  protected
  def layout
    false if request.xhr?
  end

  private
  def comments_params
    params.require(:comment).permit(:name, :email, :text)
  end

end
