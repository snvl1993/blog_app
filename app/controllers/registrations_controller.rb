include ApplicationHelper
class RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters, if: :devise_controller?

  def edit
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    super
  end

  def profile_image_upload
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)

    if params.include? :remove
      resource.remove_avatar!
    else
      resource.avatar = params[:user][:avatar] unless params[:user].blank? or params[:user][:avatar].blank?
    end

    resource.save

    if request.xhr?
      respond_to do |format|
          format.js
      end
    else
      redirect_to :back
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :username, :email, :password, :password_confirmation) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :username, :email, :password, :password_confirmation, :current_password) }
  end
end