class ArticlesController < ApplicationController
  def index
    @blog = Blog.find(params[:blog_id])
    @articles = @blog.articles.page params[:page]
  end

  def show
    @article = Article.find params[:id]
    @comments = @article.comments.page(params[:page])
  end

  def new
    if can? :create, Article
      @user_blogs = editable_blogs

      raise 'You have no blogs!' if @user_blogs.blank?

      @article = Article.new
    else
      raise CanCan::AccessDenied.new('You are not permitted to create articles!', :create, Article)
    end

    if params.include? :blog_id
      @blog = Blog.find params[:blog_id]
      if cannot? :update, @blog
        raise CanCan::AccessDenied.new('You are not permitted to create articles in this blog!', :create, Article)
      end

      @article.blog_id = @blog.id
    end
  end

  def create
    @blog = Blog.find(params[:article][:blog])
    if can? :update, @blog
      @article = @blog.articles.create(article_params)
      redirect_to blog_path(@blog)
    else
      raise CanCan::AccessDenied.new('You are not permitted to create articles in this blog!', :create, Article)
    end
  end

  def edit
    @article = Article.find(params[:id])
    unless can? :edit, @article
      raise CanCan::AccessDenied.new('You are not permitted to update this article!', :create, Article)
    end

    @user_blogs = editable_blogs

    raise 'You have no blogs!' if @user_blogs.blank?
  end

  def update
    @article = Article.find(params[:id])
    @article.blog = Blog.find params[:article][:blog]

    if can? :update, @article
      if @article.update(article_params)
        redirect_to @article
      else
        @user_blogs = editable_blogs

        render 'edit'
      end
    else
      raise CanCan::AccessDenied.new('You are not permitted to update this article!', :create, Article)
    end
  end

  def destroy
    @article = Article.find(params[:id])

    if can? :destroy, @article
      @article.destroy
    else
      raise CanCan::AccessDenied.new('You are not permitted to destroy this article!', :destroy, @article)
    end

    redirect_to url_for @article.blog
  end

  private
  def article_params
    # Exclude tag ids that are not allowed for current user.
    user_tags = Tag.allowed_for_user current_user
    params[:article][:tag_ids].select! do |id|
      user_tags.exists? id
    end

    params.require(:article).permit(:title, :text, :image, tag_ids: [])
  end

  def editable_blogs(user = current_user)
    if user.ability.can? :update, Blog, ['user_id != ?', user.id]
      Blog.all.select { |blog| can? :update, blog }
    else
      user.blogs
    end
  end

  def remove_image!
    @article.remove_image!
  end
end
