class MarksController < ApplicationController
  def handle
    old_mark_button = false
    if params.include? :id
      # Find old mark or render marks if it does not exist.
      begin
        old_mark = Mark.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        return render(partial: 'marks/object_marks', locals: { object: markable })
      end

      # Memorize old mark's button value.
      old_mark_button = old_mark.clicked_button

      # Destroy old mark.
      destroy
    end

    # Create new mark if old mark was with the same clicked button.
    if !old_mark_button or !params.include? old_mark_button
      create
    end

    if request.xhr?
      object = params[:markable_type].to_s.classify.constantize.find params[:markable_id]
      count = Mark.object_marks_amounts(object, params[:mark_type])[params[:mark_type].to_sym]
      render partial: 'marks/mark_list_item', locals: { object: object, mark_type: params[:mark_type], count: count, mark: @mark }
    else
      redirect_to :back
    end
  end

  def create
    if can? :create, Mark
      @mark = Mark.new mark_params
      @mark.user_id = current_user.id

      @mark.clicked_button = mark_properties(params[:mark_type], :buttons).detect { |key_value| params.include? key_value[0] }[0]

      @mark.save
    else
      raise CanCan::AccessDenied.new('You are not permitted to create mark!', :create, Mark)
    end

    @mark
  end

  def destroy
    if params.include? :id
      mark = Mark.find(params[:id])
      if can? :destroy, mark
        mark.destroy
      else
        raise CanCan::AccessDenied.new('You are not permitted to destroy this mark!', :create, Mark)
      end
    end
  end

  private
  def mark_params
    params.permit(:markable_id, :markable_type, :mark_type)
  end

  def mark_properties(*args)
    Mark.mark_properties *args
  end

  def markable
    params[:markable_type].to_s.classify.constantize.find(params[:markable_id])
  end
end