class TagsController < ApplicationController
  def index
    @tags = Tag.allowed_for_user(current_user).order(:id).includes(:taggable)
  end

  def show
    @tag = Tag.find(params[:id])
    @taggables = @tag.taggable
  end

  def new
    if can? :create, Tag
      @tag = Tag.new
    else
      raise CanCan::AccessDenied.new('You are not permitted to create tags!', :create, Tag)
    end
  end

  def create
    if can? :create, Tag
      # Unauthenticated user cannot set tag status.
      params[:tag].delete :status if cannot? :update, Tag

      @tag = Tag.new tag_params
      @tag.user = current_user

      if @tag.save
        redirect_to tags_path
      else
        render 'form'
      end
    else
      raise CanCan::AccessDenied.new('You are not permitted to create tags!', :create, Tag)
    end
  end

  def edit
    if can? :update, Tag
      @tag = Tag.find params[:id]
    else
      raise CanCan::AccessDenied.new('You are not permitted to update tags!', :update, Tag)
    end
  end

  def update
    # If certain button is clicked, do appropriate action.
    if params.include? :approve
      approve
    elsif params.include? :disapprove
      disapprove
    elsif params.include? :remove
      destroy
    end

    # If params include tag element, update tag in DB.
    if params.include? :tag
      processor :update do |tag|
        tag.update tag_params
      end
    end
  end

  def approve
    processor :update do |tag|
      tag.status = true
      tag.save
    end
  end

  def disapprove
    processor :update do |tag|
      tag.status = false
      tag.save
    end
  end

  def destroy
    processor :destroy do |tag|
      tag.destroy
    end
  end

  private

  def processor(permission)
    tag = Tag.find params[:id]
    if can? permission, tag
      # Do something with tag.
      yield tag
    else
      raise CanCan::AccessDenied.new('You are not permitted to administer tags!', permission, tag)
    end

    redirect_to tags_path
  end

  def tag_params
    params.require(:tag).permit(:title, :status)
  end
end
