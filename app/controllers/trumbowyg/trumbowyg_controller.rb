class Trumbowyg::TrumbowygController < ApplicationController
  def file_upload
    if params.include? :fileToUpload
      asset = Trumbowyg::Asset.new
      asset.file = params[:fileToUpload]

      # Try to find file with the same hash.
      # It is likely the same file.
      old_asset = Trumbowyg::Asset.find_by_digest asset.digest
      if old_asset
        # Do not upload new file if the same has been found.
        # Use old file.
        filename = old_asset.file.full_width.url
      else
        asset.save
        filename = asset.file.full_width.url
      end

      respond_to do |f|
        f.json do
          render json: {
              file: filename,
              message: :uploadSuccess
          }
        end
      end
    end
  end
end
