class BlogsController < ApplicationController
  def index
    @blogs = Blog.all
  end

  def show
    @blog = Blog.find params[:id]
    @articles = @blog.articles.page(params[:page])
  end

  def new
    if can? :create, Blog
      @blog = Blog.new
    else
      raise CanCan::AccessDenied.new('You are not permitted to create blogs!', :create, Blog)
    end
  end

  def edit
    @blog = Blog.find(params[:id])
    unless can? :edit, @blog
      raise CanCan::AccessDenied.new('You are not permitted to update this blog!', :create, Blog)
    end
  end

  def update
    @blog = Blog.find(params[:id])

    if can? :update, @blog
      if @blog.update(blog_params)
        redirect_to @blog
      else
        render 'edit'
      end
    else
      raise CanCan::AccessDenied.new('You are not permitted to update this blog!', :create, Blog)
    end
  end

  def create
    if can? :create, Blog
      @blog = Blog.new blog_params
      @blog.user = current_user

      if @blog.save
        redirect_to @blog
      else
        render 'new'
      end
    else
      raise CanCan::AccessDenied.new('You are not permitted to create blogs!', :create, Blog)
    end
  end

  def destroy
    @blog = Blog.find(params[:id])

    if can? :destroy, @blog
      @blog.destroy
    else
      raise CanCan::AccessDenied.new('You are not permitted to destroy this blog!', :destroy, @blog)
    end

    redirect_to blogs_path
  end

  private
  def blog_params
    params.require(:blog).permit(:title, :description)
  end
end
