include ApplicationHelper

class UsersController < ApplicationController
  def profile
    if params.include? :user_id
      @user = User.find params[:user_id]
    else
      @user = current_user
    end
  end
end
