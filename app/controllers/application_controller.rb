class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?

  # Method that decides need to show peek bar or not
  def peek_enabled?
    # Only users who can access peek can see it in production.
    # But everybody can see peek development environment.
    can?(:access, 'Peek') || [:development].include?(Rails.env.to_sym)
  end

  rescue_from ActiveRecord::RecordNotFound do
    render_404
  end

  rescue_from CanCan::AccessDenied do |e|
    render_403 e
  end

  rescue_from RoleAssignment::RoleAssignmentError do |exception|
    flash[:error] = exception.to_s
    redirect_to :back
  end

  def render_404
    render file: "#{Rails.root}/public/404.html", layout: false, status: 404
  end

  def render_403(e)
    render partial: 'error_403', locals: {error: e}, layout: false, status: 403
  end

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) {|u| u.permit(:email, :password, :password_confirmation, roles: [])}
  end

  class << self
    attr_reader :parent_resources_list

    def parent_resources(*parents)
      @parent_resources_list = parents
    end
  end

  def parent_id(parent)
    request.path_parameters["#{ parent.to_s }_id".to_sym]
  end

  def parent_type
    self.class.parent_resources_list.detect { |parent| parent_id(parent) }
  end

  def parent_class
    parent_type && parent_type.to_s.classify.constantize
  end

  def parent_object
    parent_class && parent_class.find_by_id(parent_id(parent_type))
  end
end