require 'digest'
class Trumbowyg::Asset < ActiveRecord::Base
  self.table_name = :trumbowyg_assets

  mount_uploader :file, TrumbowygUploader

  ### Digest
  #
  # Assume the model has `digest` column
  #
  # When Reading image, the local file does NOT exist,
  # And this method should return the digest from model
  #
  # When Writing image (upload), the local file DOES exist,
  # And this method should return digest calculated from file content
  def digest
    # Check if LOCAL file exists, i.e. is uploading file
    # 1. File not changed = just read from record
    # 2. File not present = How to generate digest?
    # 3. File has no path = Same as #2
    # 4. File is not local = The file is uploaded, not uploading, this check does not work if file are uploaded locally though
    if file_changed? &&
        file.file.present? &&
        file.file.respond_to?(:path) &&
        File.exists?(file.file.path)
      Digest::SHA1.new.file(file.file.path).hexdigest
    else # Reading imageimage.file.path
      self[:digest]
    end
  end

  before_save :update_digest

  protected

  def update_digest
    self.digest = digest if file_changed?
  end
end