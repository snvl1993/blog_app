class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities


    # can :manage, :all if user.is? :admin
    @user = user || User.new
    @user.roles.each do |role|
      method = role.to_sym
      if respond_to? method
        send(method)
      end
    end

    if user.blank?
      can :read, :all #for guest without roles
    end

    if (@user.roles.size == 0) && (!user.blank?)
      can :create, Blog
      can :update, Blog, user_id: @user.id
      can [:update, :create], Article, blog: {user_id: @user.id}
      can [:create, :destroy], Mark, user_id: @user.id
      can :create, Tag
    end
  end

  def moderator
    can [:edit, :destroy], Comment
    can :update, Tag
  end

  def admin
    can :access, 'Peek'
    can :access, :rails_admin
    can :dashboard
    can :manage, :all
    can :assign_roles, User
  end
end
