class Role < ActiveRecord::Base
  validates :name, presence: true

  has_many :role_assignments, dependent: :destroy
  has_many :users, :through => :role_assignments

  def to_sym
    name.downcase.gsub(/\s+/, '_').underscore.to_sym
  end
end
