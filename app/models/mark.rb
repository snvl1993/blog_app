class Mark < ActiveRecord::Base
  belongs_to :markable, polymorphic: true
  belongs_to :user

  before_save do
    mark = Mark.find_by(user_id: user_id, markable_id: markable_id, markable_type: markable_type, mark_type: mark_type)

    mark.destroy unless mark.blank?
  end

  class << self
    @model_associations = {}
    @properties = {}

    def object_marks(object_or_class, id = nil)
      if id.blank? and !(object_or_class.is_a? String or object_or_class.is_a? Symbol)
        id = object_or_class.id
        type = object_or_class.class.to_s
      else
        type = object_or_class.to_s
      end

      where(markable_id: id, markable_type: type)
    end

    def user_marks(user)
      return where(user_id: -1) if user.blank?

      if user.is_a? User
        where(user_id: user.id)
      elsif user.is_a? Numeric
        where(user_id: user)
      else
        raise ArgumentError, 'Argument have to be User object or numeric'
      end
    end

    def object_mark_types(object)
      @model_associations ||= Rails.configuration.mark_model_associations

      marks = @model_associations[object.model_name.element.to_sym]

      if marks.is_a? Array
        marks
      else
        [marks]
      end
    end

    def mark_properties(*args)
      # Get configuration from initializer file and write it into class variable
      # if it is first method call and @properties is blank.
      @properties ||= Rails.configuration.mark_properties

      res = @properties

      # Go deep into properties variable according to args.
      args.each do |el|
        if res.include? el.to_sym
          res = res[el.to_sym]
        else
          res = nil
          break
        end
      end

      res
    end

    # @param object - can be <tt>ActiveRecord::Base</tt> object, <tt>Array</tt> [class_name, id] or <tt>Hash</tt> {class: class_name, id: id}
    # @param mark_types - can be singe mark type or array of mark types
    # @return - <tt>Hash</tt> of mark counts keyed by mark types.
    # Counts number of marks which were created for specified object.
    # If mark_types is not specified result contains all marks types which are associated with current object, else it contains only types from parameter.
    def object_marks_amounts(object, mark_types = [])
      mark_values = {}

      # Prepare parameters.
      if object.is_a? Hash and object.include? :class and object.include? :id
        object = object[:class].to_s.classify.constantize.find object[:id]
      elsif object.is_a? Array and object.count == 2
        object = object[0].to_s.classify.constantize.find object[1]
      end

      mark_types = [mark_types] unless mark_types.is_a? Array
      mark_types = object_mark_types(object) if mark_types.blank?

      # Initialize mark_values for each mark type.
      mark_types.each do |el|
        mark_values[el.to_sym] = []
      end

      # Find object marks.
      marks = object_marks(object).where(mark_type: mark_types)

      # Set mark values for each mark.
      # Value depends on clicked button.
      marks.each do |mark|
        value = mark_properties mark.mark_type, :buttons, mark.clicked_button.to_sym, :value
        unless value.blank?
          mark_values[mark.mark_type.to_sym].push value
        end
      end

      # Count final value for each mark type.
      mark_values.each_pair do |mark, values|
        mark_values[mark] = 0
        if values.present?
          case mark_properties mark, :counting_method
          when :sum
            mark_values[mark] = values.sum
          when :average
            mark_values[mark] = values.sum / values.size.to_f
          else
          end
        end
      end

      mark_values
    end
  end
end
