include ApplicationHelper

class Article < ActiveRecord::Base
  belongs_to :blog
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :marks, as: :markable, dependent: :destroy
  has_many :tags_associations, as: :taggable, dependent: :destroy
  has_many :tags, through: :tags_associations

  mount_uploader :image, ArticleImageUploader

  paginates_per 3

  before_save do
    self.text = sanitize_content(self.text)
  end

  validates :title, presence: true,
            length: { minimum: 5 }
end
