include ApplicationHelper

class Blog < ActiveRecord::Base
  has_many :articles, dependent: :destroy
  belongs_to :user

  before_save do
    self.description = sanitize_content(self.description)
  end

  validates :title, presence: true,
            length: { minimum: 5 }
end
