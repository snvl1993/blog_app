include ApplicationHelper

class Comment < ActiveRecord::Base
  paginates_per 5

  belongs_to :user
  belongs_to :commentable, polymorphic: true
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :marks, as: :markable, dependent: :destroy

  default_scope { order(created_at: :desc) }

  before_save do
    self.text = sanitize_content(self.text)
  end

  validates :name, presence: true,
            length: {minimum: 3}, if: 'user_id.blank?'
  validates :text, presence: true

  def number
    id.to_s.rjust(5, '0')
  end
end
