class RoleAssignment < ActiveRecord::Base
  belongs_to :user
  belongs_to :role

  before_destroy :disable_own_admin_role_removal

  protected
  def disable_own_admin_role_removal
    # If user is admin he is not allowed to remove his admin role
    user = User.find_by_id user_id
    if user.has_role?(:admin)
      role = Role.find_by_id role_id

      if role.to_sym == :admin
        raise RoleAssignmentError, 'You cannot remove your own admin role!'
      end
    end
  end

  class RoleAssignmentError < RuntimeError
  end
end
