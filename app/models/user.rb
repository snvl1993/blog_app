require 'carrierwave/orm/activerecord'

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :role_assignments, dependent: :destroy
  has_many :roles, through: :role_assignments, dependent: :destroy
  has_many :blogs
  has_many :articles, through: :blogs
  has_many :comments
  has_many :marks

  validates :username, presence: true,
            length: { minimum: 3 }, uniqueness: true

  mount_uploader :avatar, AvatarUploader

  def has_role?(role_sym)
    roles.any? { |r| r.to_sym == role_sym }
  end

  def ability
    @ability ||= Ability.new(self)
  end
end
