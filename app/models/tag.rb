class Tag < ActiveRecord::Base
  has_many :tags_associations, dependent: :destroy
  has_many :taggable, through: :tags_associations, source: :taggable, source_type: 'Article'
  belongs_to :user

  # Select only approved tags (status = true) or tags that belongs to user.
  # If user can manage tags do nothing.
  def self.allowed_for_user(user)
    if user
      if user.ability.can? :manage, Tag
        all
      else
        where('user_id = :id or status = :status', id: user.id, status: true)
      end
    else
      where(status: true)
    end
  end

  def approved?
    status
  end
end
