module PaginationHelper
  def pagination_type
    if session[:pagination_type].blank?
      :infinite
    else
      session[:pagination_type].to_sym
    end
  end

  def infinite_pagination?
    if pagination_type.blank?
      true
    else
      pagination_type.to_sym == :infinite
    end
  end

  def no_more(collection = nil)
    raw 'There is nothing more to show!<span class="badge">Up</span>'
  end

  def pagination_links(options, params)
    no_more = (options.include? :no_more) ? options[:no_more] : nil

    # Override params argument if block is given.
    params = yield if block_given?

    if options.include? :collection
      collection = options[:collection]

      if infinite_pagination?
        if collection.last_page?
          render_nothing_to_paginate no_more, collection
        else
          paginate collection, theme: 'infinity', params: params
        end
      else
        paginate collection, theme: 'twitter-bootstrap-3'
      end
    else
      render_nothing_to_paginate no_more
    end
  end

  def render_nothing_to_paginate(callback = nil, collection = nil)
    if lookup_context.find_all('application/_infinity_scroll_last_page').any?
      text = (!callback.blank? and respond_to? callback) ? send(callback, collection) : nil
      render partial: 'application/infinity_scroll_last_page', locals: {text: text}
    end
  end

  def pagination_items(collection, type, options, paginator_options = {}, &block)
    locals = {block: block, collection: collection, paginator_options: paginator_options}
    add_attr options, :html_class, 'infinite' if infinite_pagination?
    add_attr locals, :html_class, options[:html_class] unless options[:html_class].blank?
    case type
      when :table
        add_attr locals, :header, options[:header] unless options[:header].blank?
        render 'pagination/table', locals
      when :ul
        render 'pagination/ul', locals
      when :ol
        render 'pagination/ol', locals
      else
        ''
    end
  end
end