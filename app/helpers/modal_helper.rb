module ModalHelper
  def modal_window(options)
    if options.include? :ajax
      ajax_path = url_for options[:ajax]

      options[:data] ||= {}
      options[:data][:ajax_path] = ajax_path

      modal = render layout: '/application/modal/modal', locals: options do |region|
        case region.to_sym
          when :header then
            'Wait please...'
          when :body then
            link_to '', ajax_path, remote: true, class: 'modal-ajax'
          else
        end
      end
    else
      modal = render layout: '/application/modal/modal', locals: options do |region|
        yield region.to_sym
      end

    end

    content_for :modals do
      modal
    end
  end
end