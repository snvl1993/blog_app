module MarksHelper
  def mark_form_partial(type)
    if type.is_a? Mark
      type = type.mark_type
    end

    "#{type.to_s}"
  end
end
