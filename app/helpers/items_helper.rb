module ItemsHelper
end

module ActionView
  module Helpers
    class FormBuilder
      extend ActiveSupport::Concern

      def trumbowyg_area(method, options = {})
        options = add_attr options, :class, 'trumbowyg'

        @template.send("text_area", @object_name, method, objectify_options(options))
      end
    end
  end
end