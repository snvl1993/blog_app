include ActionView::Helpers::SanitizeHelper

module ApplicationHelper
  def sanitize_content(content)
    sanitize(content, tags: BlogApplication::ALLOWED_TAGS, attributes: BlogApplication::ALLOWED_ATTRIBUTES)
  end

  def admin_links(object)
    links = []
    links.push link_to('edit', edit_polymorphic_path(object), title: 'Click to edit this item') if can? :edit, object
    if can? :destroy, object
      links.push link_to('destroy', {action: :destroy, id: object.id, controller: object.class.name.tableize}, {method: :delete, title: 'Click to destroy this item', data: {confirm: 'Are you sure?'}})
    end

    links
  end

  def add_attr(options, key, value_or_array)
    value_or_array = [value_or_array] unless value_or_array.is_a? Array

    if options.include? key
      if options[key].is_a? Array
        options[key] += value_or_array
      else
        options[key] = [options[key]] + value_or_array
      end
    else
      options[key] = value_or_array
    end

    options
  end

  # Use this helper to set page h1 header.
  def page_h1(h1)
    content_for(:h1) { h1 }
  end

  # Use this helper to set page title.
  def page_title(title)
    content_for(:title) { title }
  end

  def partial_exists?(name, prefixes = nil)
    # If prefixes variable isn't set,
    # name contains both of partial name and prefixes.
    if prefixes.blank?
      parts = name.split '/'

      # Last part is partial name.
      name = parts[-1]

      # Other parts form prefix.
      prefixes = parts[0, parts.size - 1].join '/'
    end

    lookup_context.template_exists?(name, prefixes, true)
  end
end
