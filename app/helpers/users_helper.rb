module UsersHelper
  def user_has_avatar?(user = current_user)
    !user.avatar.file.blank?
  end

  def user_avatar(user = current_user, style = :default)
    classes = [(style === false) ? 'origin' : style.to_sym, 'avatar']
    if user_has_avatar?(user) and ((style === false) or user.avatar.respond_to?(style))
      if style === false
        res = image_tag user.avatar.url, class: classes if style === false
      else
        res = image_tag user.avatar.send(style).url, class: classes if style != false and user.avatar.respond_to?(style)
      end
    else
      attrs = add_attr gravatar_attrs(user.email, style), :class, classes
      res = image_tag attrs[:src], attrs.except(:src)
    end

    res
  end

  def user_profile_path(user = current_user)
    if user == current_user
      url_for controller: 'users', action: 'profile'
    else
      url_for controller: 'users', action: 'profile', user_id: user.id
    end
  end
end
