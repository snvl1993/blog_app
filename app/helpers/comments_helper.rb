module CommentsHelper
  def comment_comments_path(comment)
    url_for(comment) + '/reply'
  end

  def new_comment_reply(comment)
    url_for(comment) + '/reply'
  end
end
