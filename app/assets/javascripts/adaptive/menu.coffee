$doc = $(document)

$doc.on 'turbolinks:load', ->
  # Extra small.
  enquire.register "#{devices.xs}",
    # Move menu to the top of <body>.
    match: ->
      $('body').triggeredPrependChild $('.row .main-menu')

  enquire.register "#{devices.sm}, #{devices.md}, #{devices.lg}",
    match: ->
      # Insert menu after open menu link.
      $('.header-content .open-menu').triggeredAfter $('.main-menu')

$doc.off('click.toggle-menu').on 'click.toggle-menu', '.open-menu', ->
  $('body').toggleClass('opened-menu')

$doc.off('click.hide-menu').on 'click.hide-menu', (e) ->
  $target = $(e.target)
  triggersSelector = '.main-menu, .open-menu'
  $('body.opened-menu').removeClass('opened-menu') unless $target.is(triggersSelector) or $target.closest(triggersSelector).length

# Disable horizontal scroll for mobile devices.
# Ovewflow property is not working on touch devices.
$doc.bind 'scroll', () ->
  if ($doc.scrollLeft() != 0) and $('.opened-menu .main-menu').length
    $doc.scrollLeft(0)
