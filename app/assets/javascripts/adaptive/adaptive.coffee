# Set variable containing all screen settings for different devices.
window.devices = {
  xs: '(min-width: 0) and (max-width: 767px)'
  sm: '(min-width: 768px) and (max-width: 991px)'
  md: '(min-width: 992px) and (max-width: 1199px)'
  lg: '(min-width: 1200px)'
}

# Run each header on page load.
# This code is needed for execution unmatch code on page load,
# For instanse if the mobile markup is shown on page load but client is desktop,
# it should be rebuild for his device.
$(document).on 'turbolinks:load', ->
  context = document
  $.each enquire.queries, (index, value) ->
    if(value.mql.matches == true)
      $.each value.handlers, (hi, hv) ->
        if(hv.options.unmatch != undefined)
          hv.options.unmatch(context)

        if(hv.options.match != undefined)
          hv.options.match(context)