class Collapsable
  settings:
    itemsNumber: 5,
    hideText: 'Hide',
    showText: 'Show more'

  constructor: (@element, options) ->
    @settings = $.extend({}, @settings, options)

    $this = @element
    if $this.children().length > @settings.itemsNumber
      $this = $this.addClass('collapsable collapsed')
      $this.find(":nth-child(#{@settings.itemsNumber}) ~ *").addClass('toggable')
      $this.append('<a class="show-all"><span class="icon"></span><span class="text">' + @getLinkText($this) + '</span></a>')

    handler = (=> @linkHandler())
    $('.show-all', @element).on 'click.limitedList', handler

  linkHandler: ->
    @element.toggleClass('collapsed')
    $('.text', @element).text(@getLinkText(@element))

  getLinkText: ($list) ->
    if $list.hasClass('collapsed') then @settings.showText else @settings.hideText


$.fn.extend collapsable: (options) ->
  $(@).each ->
    $(@).data('collapsable', new Collapsable($(@), options))