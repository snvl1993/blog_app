#= require trumbowyg/trumbowyg
#= require trumbowyg/langs/ru
#= require trumbowyg/plugins/upload/trumbowyg.upload

$(document).on "turbolinks:load", () ->
  $('.trumbowyg').trumbowyg(
    svgPath: '/assets/trumbowyg/images/icons.svg',
    lang: 'ru'
    semantic: true,
    btns: [
      ['viewHTML'],
      ['undo', 'redo'],
      ['formatting'],
      'btnGrp-semantic',
      ['superscript', 'subscript'],
      ['link'],
      ['insertImage', 'upload'],
      'btnGrp-justify',
      'btnGrp-lists',
      ['horizontalRule'],
      ['removeformat'],
      ['fullscreen']
    ],
    btnsDef: {
      formatting: {
        dropdown: ['p', 'blockquote', 'h3', 'h4'],
        ico: 'p'
      }
    },
    plugins: {
      upload: {
        serverPath: Routes.trumbowig_file_upload_path(),
        statusPropertyName: 'file'
      }
    }
  ).on('tbwopenfullscreen', () ->
    $('#page-header').hide()
  ).on('tbwclosefullscreen', () ->
    $('#page-header').show()
  )
