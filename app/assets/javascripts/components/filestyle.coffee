namespace = 'filestyle'

initFilestyles = (context = document) ->
  $(':file', context).each ->
    data = $(@).namespacedData('filestyle')

    camelCased = {}
    $.each data, (k, v) ->
      camelCased[$.camelCase k] = v

      # Return true because if v is false, the each iterator will be determine.
      true

    $(@).filestyle($.extend(input: false, camelCased))

$(document).on $.namespace('turbolinks:load', namespace), () ->
  initFilestyles()

$(document).on $.namespace('after_insertion', namespace), (e, el) ->
  initFilestyles(el)