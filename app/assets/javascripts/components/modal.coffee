$(document).on 'show.bs.modal', '.modal', (e) ->
  $this = $(@)
  name = $this.attr('name')

  # Set hash according to the modal window.
  location.hash = "##{name}" if name

  path = $this.data 'ajaxPath'

  $.get path, {}, (data) ->
    $this.find(".modal-content").triggeredReplaceWith(data)

$(document).on 'hide.bs.modal', '.modal', (e) ->
  # Clear hash when modal is closed.
  location.hash = ''

$(document).on 'ready.modal', () ->
  hashChangeHandler()

$(window).on 'hashchange.modal', () ->
  hashChangeHandler()

hashChangeHandler = () ->
  name = parseHash(location.hash)

  # Search modal with name according to current hash
  # and show it.
  $(".modal[name=#{name}").modal('show') if name

parseHash = (hash) ->
  # Remove "#" from string.
  hash.substring(1, hash.length)