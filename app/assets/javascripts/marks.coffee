namespace = 'marks'
formSel = ".mark-item form"

setAjaxHandlers = (context = document) ->
  $(context)
  .off($.namespace('ajax:success', namespace), formSel)
  .off($.namespace('ajax:error', namespace), formSel)
  .on($.namespace('ajax:success', namespace), formSel, (e, data, status, xhr) ->
    $(this).closest('.mark-item').triggeredReplaceWith xhr.responseText
  )
  .on($.namespace('ajax:error', namespace), formSel, (e, data, status, xhr) ->
    $errors = $(this).closest('.mark-item').find('.error')
    error = "<div class='error'>#{data.statusText}</div>"

    if $errors.length
      $errors.triggeredReplaceWith error
    else
      $(this).closest('.mark-item').triggeredAppendChild error
  )

$(document).on "turbolinks:load", =>
  setAjaxHandlers()

# Bind handler that sets hover class on marks wrapper.
# That is useful for styling.
$(document).on 'mouseenter', '.marks button', -> $(@).closest('.mark-item').addClass('hover')
$(document).on 'mouseleave', '.marks button', -> $(@).closest('.mark-item').removeClass('hover')