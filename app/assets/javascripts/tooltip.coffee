class Tooltips
  settings: {}

  constructor: (context = document, @tooltipSel, settings) ->
    @settings = $.extend({}, @settings, settings)

    @initTooltips(context)

  destroyTooltips: (context = document) ->
    $(@tooltipSel, context).tooltip('destroy')

  initTooltips: (context = document) ->
    $(@tooltipSel, context).tooltip(@settings)

$.fn.extend customTooltip: (tooltipSel, options) ->
  namespace = 'infinite'

  tooltip = new Tooltips(this, tooltipSel, options)

  # Remove tooltips on elements that will be removed soon.
  $(document)
  .off($.namespace('before_removal', namespace))
  .on($.namespace('before_removal', namespace), (e, el) ->
    tooltip.destroyTooltips(el)
  )

  # Init tooltips on elements that just have been inserted.
  $(document)
  .off($.namespace('after_insertion', namespace))
  .on($.namespace('after_insertion', namespace), (e, el) ->
    tooltip.initTooltips(el)
  )