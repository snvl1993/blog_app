# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file.
#
# Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
# about supported directives.
#
#= require jquery
#= require jquery_ujs
#= require jquery.infinite-pages
#= require turbolinks
#= require js-routes
#= require peek
#= require peek/views/performance_bar
#= require bootstrap-sprockets
#= require bootstrap-filestyle
#= require jquery.remotipart
#= require enquirejs/enquire
#= require_tree ./components
#= require users
#= require_tree .

$(document).on "turbolinks:load", () ->
  $('.infinite.pagination-wrapper').infinite
    plugin: {
      debug: true
      buffer: 500
      loading: ->
        $(this).text("Loading...")
      error: ->
        $(this).text("Trouble! Please drink some coconut water and click again")
    }

  $(document).customTooltip('[title]', {placement: 'auto bottom', container: 'body'})

  # If form element has class 'autosubmit' it have to trigger form submittion on 'change' event.
  autosubmitSel = 'form .autosubmit, form.autosubmit *'
  $(document).off($.namespace('change', 'autosubmit'), autosubmitSel)
  .on($.namespace('change', 'autosubmit'), autosubmitSel, () ->
    $(this).closest('form').submit()
  )

  setCollapsable()

$(document).on 'after_insertion', (e, el) ->
  setCollapsable el

$(document).on 'scroll', (e) ->
  $body = $ 'body'
  if $(window).scrollTop() == 0
    $body.removeClass('scrolled')
  else
    $body.addClass('scrolled')

setCollapsable = (context = document) ->
  $('.tags-list', context).collapsable()

$('.dropdown-submenu').click (e) ->
  e.preventDefault()
  $(@).children().filter('.dropdown-submenu > .dropdown-menu').css('display', 'block')
  false