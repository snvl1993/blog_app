class Infinite
  # Selectors defaults.
  settings:
    selectors: {
      navWrapper: '.pagination.infinity-pagination',
      nav: '.next a',
      content: '.content'
    }

  constructor: (@element, options) ->
    @settings = $.extend({}, @settings, options)

    @settings.plugin.navSelector = @settings.selectors.navWrapper + ' ' + @settings.selectors.nav

    # Initiate infinitePages plugin.
    $(@element).infinitePages @settings.plugin

  replaceContent: (data) ->
    $data = $(data)

    # Find content for insertion.
    contentSel = @settings.selectors.content
    $content = if ($data.is(contentSel)) then $data.filter(contentSel) else $data.find(contentSel)

    # Replace navigation with new content.
    $(@element).find(@settings.selectors.navWrapper).triggeredReplaceWith $content.html()

  stop: () ->
    # @todo Unbind only infinite pages scroll event.
    context = @settings.plugin.context || window
    $(context).off 'scroll'

$.fn.extend infinite: (options) ->
  namespace = 'infinite'
  self = @

  infinite = new Infinite(@, options)

  # Event handler which replaces old navigation with new content and navigation.
  # Event should be trigerred in action.js view. Data should contain html of new content.
  $(document).off($.namespace('infinite:load', namespace)).on $.namespace('infinite:load', namespace), (e, data) ->
    infinite.replaceContent(data)

  $(document).off($.namespace('turbolinks:visit', namespace)).on $.namespace('turbolinks:visit', namespace), () ->
    infinite.stop()