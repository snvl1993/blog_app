$.fn.triggeredReplaceWith = () ->
  $content = $(arguments[0])

  $(document).trigger 'before_removal', @
  $(document).trigger 'before_insertion', [$content]

  @.replaceWith $content

  $(document).trigger 'document:change'
  $(document).trigger 'after_insertion', [$content]
  $(document).trigger 'after_removal', @

$.fn.triggeredAppendChild = () ->
  addElement.call @, 'append', arguments[0]

$.fn.triggeredPrependChild = () ->
  addElement.call @, 'prepend', arguments[0]

$.fn.triggeredAfter = () ->
  addElement.call @, 'after', arguments[0]
  
$.fn.triggeredBefore = () ->
  addElement.call @, 'before', arguments[0]
  
addElement = (callback) ->
  $content = $(arguments[1])

  @[callback].call @, $content

  $(document).trigger 'document:change'
  $(document).trigger 'after_insertion', [$content]