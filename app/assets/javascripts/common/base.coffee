$.extend namespace: (event, namespace) ->
  if namespace
    "#{event}.#{namespace}"
  else
    event

$.extend camelCase: (input) ->
  input.toLowerCase().replace(/-(.)/g, (match, g) ->
    g.toUpperCase()
  )

$.fn.attributes = ->
  attrs = {}

  $.each @.get(0).attributes, (i, attr) ->
    attrs[attr.name] = attr.value
    true

  attrs

$.fn.namespacedData = (namespace) ->
  attrs = @.attributes()
  data = {}
  $.each attrs, (attr, val) ->
    prefix = "data-#{namespace}-"
    if attr.substr(0, prefix.length) == prefix
      # Change string value false to boolean.
      val = if val == 'false' then false else val
      data[attr.substr(prefix.length)] = val

      # Return true because if val is false, the each iterator will be determine.
      true
  data