# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.coffee.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'ajax:success', '.get-comment-form', (e, data) ->
  $('#new_comment').remove()
  $target = $(@).closest('.comment-body')
  if $target.length
    $target.after(data)
  else
    $(@).closest('.entity').find('.comments').triggeredPrependChild(data)

$(document).on 'ajax:success', '#new_comment', (e, data) ->
  $data = $(data)
  $(@).closest('.comment, .comments').triggeredReplaceWith(data) if $data.find('.comment, .comments').length
  $(@).triggeredReplaceWith(data) if $data.is('#new_comment')