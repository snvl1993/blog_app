require 'test_helper'

class MarksControllerTest < ActionController::TestCase
  test 'unauthenticated cannot create mark' do
    # Prepare mark parameters.
    object = Article.first
    mark_type = Mark.object_mark_types(object).first
    button = Mark.mark_properties(mark_type, :buttons).first.first

    # Try to create new mark.
    post :handle, markable_id: object.id, markable_type: object.class.to_s, mark_type: mark_type, button.to_sym => 1

    # Should be 403, access denied.
    assert_response 403
  end

  test 'unauthenticated cannot destroy mark' do
    # Prepare mark parameters.
    object = Article.first
    mark_type = Mark.object_mark_types(object).first
    button = Mark.mark_properties(mark_type, :buttons).first.first

    # Try to destroy mark.
    post :handle, markable_id: object.id, markable_type: object.class.to_s, mark_type: mark_type, button.to_sym => 1, id: Mark.first.id

    # Should be 403, access denied.
    assert_response 403
  end

  test 'authenticated can create mark' do
    # Set referer header, because controller should redirect back.
    request.env['HTTP_REFERER'] = request.base_url

    # Sign in.
    sign_in user_without_roles

    # Prepare mark parameters.
    object = Article.first
    mark_type = Mark.object_mark_types(object).first
    button = Mark.mark_properties(mark_type, :buttons).first.first

    # Memorize marks count before creation of the new one.
    marks_count = Mark.count

    # Try to create mark.
    post :handle, markable_id: object.id, markable_type: object.class.to_s, mark_type: mark_type, button.to_sym => 1

    # Response code should be redirect.
    assert_response :redirect

    # After mark creation mark count should raise.
    assert_equal marks_count + 1, Mark.count
  end

  test 'authenticated cannot destroy another user mark' do
    # Set referer header, because controller should redirect back.
    request.env['HTTP_REFERER'] = request.base_url

    # Sign in.
    sign_in user_without_roles

    # Search another user mark.
    mark = another_user_item current_user, :marks

    # Try to destroy another user mark.
    post :handle, markable_id: mark.markable.id, markable_type: mark.markable.class.to_s, mark_type: mark.mark_type, mark.clicked_button => 1, id: mark.id

    # Should be 403, access denied.
    assert_response 403
  end

  test 'authenticated can destroy own mark' do
    # Set referer header, because controller should redirect back.
    request.env['HTTP_REFERER'] = request.base_url

    # Sign in.
    sign_in user_without_roles

    # Prepare mark parameters.
    object = Article.first
    mark_type = Mark.object_mark_types(object).first
    button = Mark.mark_properties(mark_type, :buttons).first.first

    # Create new mark.
    mark = Mark.new markable_id: object.id, markable_type: object.class.to_s, mark_type: mark_type, clicked_button: button.to_sym, user_id: current_user.id
    mark.save

    # Memorize marks count before creation of the new one.
    marks_count = Mark.count

    # Memorize mark id
    mark_id = mark.id

    # Try to destroy mark.
    post :handle, markable_id: mark.markable.id, markable_type: mark.markable.class.to_s, mark_type: mark.mark_type, mark.clicked_button => 1, id: mark.id

    # After mark creation mark count should decrease.
    assert_equal marks_count - 1, Mark.count

    # Mark should not exist after destroying.
    assert_raises(ActiveRecord::RecordNotFound) do
      Mark.find mark_id
    end

    # Should be redirect.
    assert_response :redirect
  end
end
