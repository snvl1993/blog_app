require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  test 'should get show' do
    get :show, id: 1
    assert_response :success
  end

  test 'should get new article comment' do
    get :new, article_id: 1
    assert_response :success
  end

  test 'should get new comment reply' do
    get :new_reply, id: 1
    assert_response :success
  end

  test 'should create article comment' do
    assert_nothing_raised do
      comment = Article.first.comments.create name: 'commenter', text: 'comment text', email: 'test@email.com'
      comment.save!
    end
  end

  test 'should create comment reply' do
    assert_nothing_raised do
      comment = Comment.first.comments.create name: 'commenter', text: 'comment text', email: 'test@email.com'
      comment.save!
    end
  end

  test 'should not create comment with invalid data' do
    # Empty name
    assert_raise ActiveRecord::RecordInvalid do
      comment = Article.first.comments.create text: 'comment text', email: 'test@email.com'
      comment.save!
      end

    # Too short name
    assert_raise ActiveRecord::RecordInvalid do
      comment = Article.first.comments.create name: 'c', text: 'comment text', email: 'test@email.com'
      comment.save!
    end

    # Empty text
    assert_raise ActiveRecord::RecordInvalid do
      comment = Article.first.comments.create name: 'commenter', email: 'test@email.com'
      comment.save!
    end
  end

end
