require 'test_helper'

class TagsControllerTest < ActionController::TestCase
  test 'admin should get new' do
    sign_in get_admin

    get :new
    assert_response :success
  end

  test 'admin should get edit' do
    sign_in get_admin

    get :edit, id: 1
    assert_response :success
  end

  test 'should get index' do
    get :index
    assert_response :success
  end

  test 'should get show' do
    get :show, id: 1
    assert_response :success
  end

  test 'guest should not have access to new' do
    get :new
    assert_response 403
  end

  test 'guest should not have access to edit' do
    get :edit, id: 1
    assert_response 403
  end

  test 'unauthenticated cannot create tag' do
    post :create, tag: { title: random_name }
    assert_response 403
  end

  test 'unauthenticated cannot update tag' do
    post :update, id: Tag.take.id, tag: { title: random_name }
    assert_response 403
  end

  test 'authenticated can create tag' do
    sign_in get_user_by_role
    tag_title = random_name
    post :create, tag: { title: tag_title, status: true }

    assert_response :redirect
    assert tag_title == Tag.last.title
    assert_not Tag.last.status, 'User without roles should not have ability to create approved tags!'
  end

  test 'admin can edit tag' do
    sign_in get_admin
    tag_id = Tag.take.id

    tag_title = random_name

    # Try to set tag title and status.
    post :update, id: tag_id, tag: { title: tag_title, status: true }

    assert_response :redirect

    tag = Tag.find(tag_id)
    assert tag_title == tag.title
    assert tag.status

    # Set tag status.
    post :update, id: tag_id, tag: { status: false }

    assert_response :redirect

    tag = Tag.find(tag_id)
    assert_not tag.status

    # Approve tag.
    post :update, id: tag_id, approve: true

    assert_response :redirect

    tag = Tag.find(tag_id)
    assert tag.status

    # Disapprove tag.
    post :update, id: tag_id, disapprove: true

    assert_response :redirect

    tag = Tag.find(tag_id)
    assert_not tag.status
  end
end
