require 'test_helper'
include PaginationHelper

class PaginationControllerTest < ActionController::TestCase
  test 'should change pagintaion type' do
    request.env['HTTP_REFERER'] = request.base_url

    get :change, {infinite: 1}
    assert_response :redirect

    assert pagination_type == :infinite
    assert_not pagination_type == :pages

    get :change, {pages: 1}
    assert_response :redirect

    assert pagination_type == :pages
    assert_not pagination_type == :infinite
  end

end
