require 'test_helper'

class BlogsControllerTest < ActionController::TestCase
  test 'index availability' do
    get :index
    assert_response :success
  end

  test 'new availability' do
    sign_in user_without_roles

    get :new
    assert_response :success
  end

  test 'show availability' do
    get :show, id: Blog.first
    assert_response :success
  end

  test 'blog which does not exist' do
    get :show, id: not_existing_id(Blog)
    assert_response :missing
  end

  test 'user should not be able to edit blog of another user' do
    sign_in user_without_roles

    get :edit, id: 1
    assert_response 403
  end

  test 'admin should be able to edit blog of another user' do
    sign_in get_admin

    get :edit, id: 2
    assert_response :success
  end
end
