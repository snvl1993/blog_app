require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  test 'index availability' do
    get :index, blog_id: Blog.first
    assert_response :success
  end

  test 'new availability when user has blogs' do
    sign_in user_with_blogs

    get :new
    assert_response :success
  end

  test 'new availability when user does not have blogs' do
    sign_in user_without_blogs

    assert_raises RuntimeError do
      get :new
    end
  end

  test 'show availability' do
    get :show, id: Article.first
    assert_response :success
  end

  test 'article which does not exist' do
    get :show, id: not_existing_id(Article)
    assert_response :missing
  end

  test 'user should not be able to edit article of another user' do
    user = user_without_roles
    sign_in user

    get :edit, id: another_user_item(user, :articles)
    assert_response 403
  end

  test 'admin should be able to edit article of another user' do
    user = get_admin
    sign_in user

    get :edit, id: another_user_item(user, :articles)
    assert_response :success
  end
end
