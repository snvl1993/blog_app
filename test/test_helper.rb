ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  include Devise::Controllers::Helpers

  def get_admin
    get_user_by_role 'Admin'
  end

  def user_without_roles
    get_user_by_role nil
  end

  # @param [String|NilClass] role_name - role which user you want to get. If you need user without roles it has to be nil.
  def get_user_by_role(role_name = nil)
    if role_name.blank?
      User.includes(:role_assignments).where(:role_assignments => {:user_id => nil}).first
    else
      User.joins(:roles).where(roles: {id: Role.find_by_name(role_name).id}).first
    end
  end

  def user_with_blogs
    User.includes(:role_assignments).where(:role_assignments => {:user_id => nil}).joins(:blogs).first
  end

  def user_without_blogs
    User.includes(:role_assignments)
        .where(:role_assignments => {:user_id => nil})
        .joins('LEFT OUTER JOIN blogs ON blogs.user_id = users.id')
        .where(blogs: {user_id: nil}).first
  end

  # @param [User] user
  # @param [Symbol] model - plural model name symbol. Example: for model Article it has to be :articles.
  def another_user_item(user, model)
    User.where.not(id: user.id).includes(model).first.send(model).first
  end

  def not_existing_id(model)
    model.order(:id).last.id + 1
  end

  def random_name(length = 8)
    (0...length).map { (65 + rand(26)).chr }.join
  end
end

class ActionController::TestCase
  # Include Devise test orders, because without it test will not work.
  include Devise::TestHelpers
end

class ActionDispatch::IntegrationTest
  include Warden::Test::Helpers
  Warden.test_mode!
end