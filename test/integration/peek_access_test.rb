require 'test_helper'

class PeekAccessTest < ActionDispatch::IntegrationTest
  test 'admin should see peek' do
    login_as get_admin, :scope => :user

    get '/'
    assert_response :success

    assert_select '#peek'
  end

  test 'unauthenticated should not see peek' do
    get '/'
    assert_response :success
    assert_select '#peek', false
  end
end
