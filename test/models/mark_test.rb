require 'test_helper'

class MarkTest < ActiveSupport::TestCase
  test 'count marks amounts' do
    markables = [
        Comment.create(name: random_name, text: random_name, commentable_id: Article.take.id, commentable_type: 'Article'),
        Article.create(title: random_name, text: random_name, blog_id: Blog.take.id)
    ]

    markables.each do |markable|
      mark_types = Mark.object_mark_types markable

      amounts = {}
      mark_types.each do |type|
        amounts[type] = []
        buttons = Mark.mark_properties type, :buttons

        # Generate random number of marks.
        rand(5).times do
          # Create test user for mark creation,
          # because each user cannot have more than one mark for each object.
          user = User.create! username: random_name,
                              confirmed_at: Time.now,
                              password: random_name,
                              email: "#{random_name}@email.com}"

          # Create mark with randomly selected buttons.
          mark = Mark.create! clicked_button: buttons.keys.sample,
                              mark_type: type,
                              markable_id: markable.id,
                              markable_type: markable.class,
                              user_id: user.id

          # Remember each mark value.
          amounts[type].push(Mark.mark_properties type, :buttons, mark.clicked_button, :value)
        end
      end

      # Count final amount values.
      amounts.each do |type, values|
        amount = 0
        if values.present?
          case Mark.mark_properties type, :counting_method
          when :sum
            amount = values.sum
          when :average
            amount = values.sum / values.size.to_f
          else
          end
        end

        amounts[type] = amount
      end

      counted_amounts = Mark.object_marks_amounts markable
      amounts.each do |type, value|
        assert counted_amounts.include?(type), "Mark '#{type.to_s}' amount is not found in result for #{markable.class.to_s} model."
        assert_equal value, counted_amounts[type], "Mark #{type.to_s} amount is counted incorrectly for #{markable.class.to_s} model."
      end
    end
  end

end
