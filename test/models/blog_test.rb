require 'test_helper'

class BlogTest < ActiveSupport::TestCase
  test 'creation' do
    blog = Blog.new title: 'Test blog', description: 'Test blog description'
    assert blog.save
  end

  test 'should not save blog without title' do
    blog = Blog.new
    assert_not blog.save
  end

  test 'should not save blog with too short title' do
    blog = Blog.new title: 'test'
    assert_not blog.save
  end

  test 'blog deletion' do
    id = 1
    while Blog.find_by_id(id)
      id += 1
    end
    puts id
    blog = Blog.new title: 'test', id: id
    blog.save

    blog.destroy
  end
end
