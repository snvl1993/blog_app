require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  test 'role should converts to symbol' do
    message = 'Incorrect role to symbol conversion'
    assert (Role.find_by_name('Admin').to_sym == :admin), message
    assert (Role.find_by_name('Authenticated author').to_sym == :authenticated_author), message
  end
end