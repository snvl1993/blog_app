require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  test 'creation' do
    article = Article.new title: 'Test article', text: 'Test article text', blog_id: Blog.first.id
    assert article.save
  end

  test 'should not save article without title' do
    article = Article.new text: 'Test article text', blog_id: Blog.first.id
    assert_not article.save
  end

  test 'should not save article with too short title' do
    article = Article.new title: 'Test', text: 'Test article text', blog_id: Blog.first.id
    assert_not article.save
  end

  test 'article deletion' do
    id = 1
    while Article.find_by_id(id)
      id += 1
    end
    article = Article.new title: 'test', id: id, blog_id: Blog.first.id
    article.save

    article.destroy
  end
end
