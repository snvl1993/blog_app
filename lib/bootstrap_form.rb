class BootstrapForm < ActionView::Helpers::FormBuilder
  include ApplicationHelper

  def text_field(attribute, options={})
    super attribute, add_attr(options, :class, 'form-control')
  end

  def select(method, choices = nil, options = {}, html_options = {}, &block)
    super method, choices, options, add_attr(html_options, :class, 'form-control'), &block
  end

  def submit(value=nil, options={})
    super value, add_attr(options, :class, %w(btn btn-default))
  end

  def text_area(method, options = {})
    super method, add_attr(options, :class, 'form-control')
  end

  def email_field(method, options = {})
    super method, add_attr(options, :class, 'form-control')
  end

  def password_field(method, options = {})
    super method, add_attr(options, :class, 'form-control')
  end

  def button(method, options = {})
    super method, add_attr(options, :class, %w(btn btn-default))
  end
end